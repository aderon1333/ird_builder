﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    public partial class IRD_Worker
    {
        private static void ReadSheetOutputParams(ExcelPackage SID, string sheetName, 
                int headerRow,
                int firstDataRow,
                string InputParameterNameColumnHeader, 
                HashSet<string> OutParams)
        {
            int OuputParamColumn = 0;
            int OuputControlColumn = 0;

            var ws_in = SID.Workbook.Worksheets[sheetName];
            for (int col = 1; col <= 50; col++)
                if (ws_in.Cells[headerRow, col].Value?.ToString().Trim() == InputParameterNameColumnHeader)
                {
                    OuputParamColumn = col;
                    break;
                }

            string InputApplicationColumnHeader = "Application";

            for (int col = 1; col <= 50; col++)
                if (ws_in.Cells[headerRow, col].Value?.ToString().Trim() == InputApplicationColumnHeader)
                {
                    OuputControlColumn = col;
                    break;
                }

            if (OuputParamColumn == 0 || OuputControlColumn ==0)
            {
                Console.WriteLine($"Column ([{InputParameterNameColumnHeader}] || {InputApplicationColumnHeader}) not found in SID [{sheetName}]");
                return;
            }

            int startRow = firstDataRow;
            int outputStartRow = IRD_Worker.GetFirstNullChannelRow(ws_in, OuputParamColumn);

            int countIn = 0;
            int countSkipped = 0;
            do
            {
                int curRowIN = startRow + countIn;

                if (ws_in.Cells[curRowIN, OuputControlColumn].Value == null)
                    break;

                var val = ws_in.Cells[curRowIN, OuputParamColumn].Value;
                if (val == null)
                {
                    countIn++;
                    continue;
                }

                countIn++;
                if (!OutParams.Add(val.ToString().Trim()))
                    countSkipped++;
            }
            while (true);

            Console.WriteLine($"SID output ports found [{sheetName}]: {countIn - countSkipped}");
        }
        private static void CollectOutputParameters(ExcelPackage SID, HashSet<string> OutParams)
        {
            OutParams.Clear();
            ReadSheetOutputParams(SID, SID_Sheets.A429_O_PageName, SID_Sheets.A429_HeaderRow, SID_Sheets.A429_FirstDataRow, "Input Parameter Name", OutParams);
            ReadSheetOutputParams(SID, SID_Sheets.AFDX_O_PageName, SID_Sheets.AFDX_HeaderRow, SID_Sheets.AFDX_FirstDataRow, "Input Parameter Name", OutParams);
            ReadSheetOutputParams(SID, SID_Sheets.Discrete_PageName, SID_Sheets.DISC_HeaderRow, SID_Sheets.DISC_FirstDataRow, "Input Parameter Name", OutParams);
        }
    }
}
