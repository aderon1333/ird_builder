﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    public partial class IRD_Worker
    {
        private static void Populate_IRD_BUS_Definition(ExcelPackage IRD, Dictionary<string, ApexPort> cH_Dict, string appName)
        {
            var ws_out = IRD.Workbook.Worksheets[IRD_Sheets.BusDef_PageName];
            Dictionary<string, int> RD = new Dictionary<string, int>();
            readBUSDefHeaders(ws_out, RD);

            int firstRow = IRD_Sheets.BusDef_FirstRow;
            int portCnt = 0;

            foreach(var pair in cH_Dict)
            {
                var row = firstRow + portCnt;
                var p = pair.Value;
                var portname = pair.Key;

                ws_out.Cells[row, RD["Partition"]].Value = appName;
                ws_out.Cells[row, RD["Identifier"]].Value = portname;
                ws_out.Cells[row, RD["I/O"]].Value = p.IO.ToString();
                ws_out.Cells[row, RD["Identity"]].Value = p.API_PortNumber;
                ws_out.Cells[row, RD["Interface Type"]].Value = p.InterfaceType;
                ws_out.Cells[row, RD["Type"]].Value = "A429";

                portCnt++;
            }

            Console.WriteLine($"BUS Definition ports created : {portCnt}");
        }

        private static void readBUSDefHeaders(ExcelWorksheet ws_out, Dictionary<string, int> headerDict)
        {
            headerDict.Clear();
            for (int col = IRD_Sheets.BusDef_HeaderColumn_First; col <= IRD_Sheets.BusDef_HeaderColumn_Last; col++)
            {
                var val = ws_out.Cells[IRD_Sheets.BusDef_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                headerDict[name] = col;
            }
        }
    }
}
