﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    public class SID_Sheets
    {
        public const string A429_O_PageName = "A429 Output Data";
        public const string A429_I_PageName = "A429 Input Data";
        public const int A429_FirstDataRow = 13;
        internal static readonly int A429_HeaderColumn_First = 2;
        internal static readonly int A429_HeaderColumn_Last = 23;
        internal static readonly int A429_HeaderRow = 12;

        internal static string AFDX_I_PageName = "Input AFDX Data Definition";
        internal static string AFDX_O_PageName = "Output AFDX Data Definition";

        internal static int AFDX_FirstDataRow = 11;
        internal static int AFDX_HeaderColumn_First = 1;
        internal static int AFDX_HeaderColumn_Last = 23;
        internal static int AFDX_HeaderRow = 10;

        internal static string AFDX_Message_PageName = "AFDX Message Definition";
        internal static int AFDX_M_FirstDataRow = 10;
        internal static int AFDX_M_HeaderColumn_First = 1;
        internal static int AFDX_M_HeaderColumn_Last = 11;
        internal static int AFDX_M_HeaderRow = 9;

        internal static string Discrete_PageName = "Discrete";
        internal static int DISC_FirstDataRow = 12;
        internal static int DISC_HeaderColumn_First = 2;
        internal static int DISC_HeaderColumn_Last = 9;
        internal static int DISC_HeaderRow = 11;

        internal static string CAN_PageName = "CAN Input Data";
        internal static int CAN_FirstDataRow = 13;
        internal static int CAN_HeaderColumn_First = 2;
        internal static int CAN_HeaderColumn_Last = 22;
        internal static int CAN_HeaderRow = 11;

        internal static string CAN_Bus_PageName = "CAN Bus";
        internal static int CAN_Bus_FirstDataRow = 14;
        internal static int CAN_Bus_HeaderColumn_First = 1;
        internal static int CAN_Bus_HeaderColumn_Last = 32;
        internal static int CAN_Bus_HeaderRow = 12;

        internal static string FileInformation_PageName = "File information";
        internal static int FileInformation_AppNameCol = 5;
        internal static int FileInformation_AppNameRow = 10;
    }
}
