﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    public partial class IRD_Worker
    {
        //const string SID_Filename = "../../../SID.xlsx";
        //const string IRD_Filename = "../../../IRD.xlsx";
        //const string IRD_Blank_Filename = "../../../IRD_Blank.xlsx";
        static private bool full664Names = false;

        static public void Do(string[] args)
        {
            string SID_Filename = "SID.xlsx";
            string IRD_Filename = "IRD.xlsx";
            string IRD_Blank_Filename = "IRD_Blank.xlsx";

            if (args.Count() == 0)
            {
                Console.WriteLine($"Usage: \n\t {Assembly.GetExecutingAssembly().GetName().Name}  <SID Filename> <IRD Filename> <IRD Blank Filename> [/silent] [/allinputs] [/full664Names]");
                return;
            }

            if (args.Count() > 0) SID_Filename = args[0];
            if (args.Count() > 1) IRD_Filename = args[1];
            if (args.Count() > 2) IRD_Blank_Filename = args[2];

            try
            {
                File.Copy(IRD_Blank_Filename, IRD_Filename, true);
            }
            catch(Exception)
            {
                Console.WriteLine("IRD Builder: couldn't copy IRD template");
            }
            
            //try
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                Console.WriteLine($"IRD Building started...");

                using (var SID = new ExcelPackage(new FileInfo(SID_Filename)))
                using (var IRD = new ExcelPackage(new FileInfo(IRD_Filename)))
                {
                    Dictionary<string, ApexPort> CH_Dict = new Dictionary<string, ApexPort>();
                    Dictionary<string, ApexPort> CH_Dict_BUS = new Dictionary<string, ApexPort>();
                    HashSet<string> AllOutputParameters = new HashSet<string>();
                    var appname = SID_GetAppName(SID);

                    if (!args.Contains("/allinputs"))
                        CollectOutputParameters(SID, AllOutputParameters);

                    if (!args.Contains("/full664Names"))
                        full664Names = true;

                    var A429BUS = new IRD_Worker_BUS(SID, IRD, CH_Dict_BUS, AllOutputParameters);
                    A429BUS.Process(true);
                    A429BUS.Process(false);

                    Populate_IRD_BUS_Definition(IRD, CH_Dict_BUS, appname);

                    var A429 = new IRD_Worker_A429(SID, IRD, CH_Dict, AllOutputParameters);
                    A429.Process(true);
                    A429.Process(false);

                    var AFDX = new IRD_Worker_AFDX(SID, IRD, CH_Dict, AllOutputParameters);
                    AFDX.Process(true);
                    AFDX.Process(false);

                    var DISC = new IRD_Worker_DISC(SID, IRD, CH_Dict, AllOutputParameters, args.Contains("/single_pp_port"));
                    DISC.Process(true);
                    DISC.Process(false);

                    var CAN = new IRD_Worker_CAN(SID, IRD, CH_Dict, AllOutputParameters);
                    CAN.Process();

                    Populate_IRD_API_Definition(IRD, CH_Dict, appname);
                    IRD.Save();
                }

                stopwatch.Stop();
                Console.WriteLine($"IRD Building complete: {stopwatch.Elapsed.TotalSeconds.ToString("F2")} sec");

                if (!args.Contains("/silent"))
                {
                    startExcelApp(IRD_Filename);
                }
                    
            }
            //catch (Exception ex)
            //{
            //    Console.WriteLine("FAILED:    " + ex.Message);
            //}
        }

        private static void startExcelApp(string IRD_Filename)
        {
            try
            {
                string fullpath = Path.GetFullPath(IRD_Filename);
                System.Diagnostics.Process.Start("excel.exe", $"/r \"{fullpath}\"");
            }
            catch (Exception)
            {
                Console.WriteLine($"IRD Builder: couldn't start excel app");
            }
        }

        private static string SID_GetAppName(ExcelPackage SID)
        {
            using (var ws_in = SID.Workbook.Worksheets[SID_Sheets.FileInformation_PageName])
            {
                return ws_in.Cells[SID_Sheets.FileInformation_AppNameRow, SID_Sheets.FileInformation_AppNameCol].Value?.ToString();
            }
        }

        internal static int GetFirstNullBUSRow(ExcelWorksheet ws_out, int row)
        {
            int res = IRD_Sheets.BusIO_FirstRow;
            do
            {
                if (ws_out.Cells[res, row].Value == null) break;
                res++;
            } while (true);

            return res;
        }
        
        internal static int GetFirstNullChannelRow(ExcelWorksheet ws_out, int row)
        {
            int res = IRD_Sheets.OutputChannel_FirstRow;
            do
            {
                if (ws_out.Cells[res, row].Value == null) break;
                res++;
            } while (true);

            return res;
        }
        internal static int GetFirstNullRow(ExcelWorksheet ws_out, int fromRow, int col)
        {
            int res = fromRow;
            do
            {
                if (ws_out.Cells[res, col].Value == null) break;
                res++;
            } while (true);

            return res;
        }
    }
}
