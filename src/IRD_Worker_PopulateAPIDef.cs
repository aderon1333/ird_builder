﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    public partial class IRD_Worker
    {
        private static void Populate_IRD_API_Definition(ExcelPackage IRD, Dictionary<string, ApexPort> cH_Dict, string appName)
        {
            var ws_out = IRD.Workbook.Worksheets[IRD_Sheets.APIDef_PageName];
            Dictionary<string, int> RD = new Dictionary<string, int>();
            readAPIDefHeaders(ws_out, RD);

            int firstRow = IRD_Sheets.APIDef_FirstRow;
            int portCnt = 0;

            var haveConnectedTo = RD.ContainsKey("Connected to");

            foreach(var pair in cH_Dict)
            {
                var row = firstRow + portCnt;
                var p = pair.Value;
                var portname = pair.Key;
                //if (full664Names && p.a664vlName != null) portname = p.a664vlName;
                ws_out.Cells[row, RD["Partition"]].Value = appName;
                ws_out.Cells[row, RD["Port Name"]].Value = portname;
                if (haveConnectedTo)
                    ws_out.Cells[row, RD["Connected to"]].Value = p.ConnectedTo;
                ws_out.Cells[row, RD["I/O"]].Value = p.IO.ToString();
                ws_out.Cells[row, RD["Port Mode"]].Value = p.Mode.ToString();
                ws_out.Cells[row, RD["Port Size"]].Value = p.PortSize;
                ws_out.Cells[row, RD["API Freq"]].Value = p.API_Freq;
                ws_out.Cells[row, RD["API Port Number"]].Value = p.API_PortNumber;
                ws_out.Cells[row, RD["Protocol type"]].Value = "API";

                portCnt++;
            }

            Console.WriteLine($"API Definition ports created : {portCnt}");
        }

        private static void readAPIDefHeaders(ExcelWorksheet ws_out, Dictionary<string, int> headerDict)
        {
            headerDict.Clear();
            for (int col = IRD_Sheets.APIDef_HeaderColumn_First; col <= IRD_Sheets.APIDef_HeaderColumn_Last; col++)
            {
                var val = ws_out.Cells[IRD_Sheets.APIDef_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                headerDict[name] = col;
            }
        }
    }
}
