﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    class IRD_Worker_A429
    {
        readonly ExcelPackage SID;
        readonly ExcelPackage IRD;
        readonly Dictionary<string, ApexPort> CH_Dict;
        readonly HashSet<string> OutputParams_Dict;

        public IRD_Worker_A429(ExcelPackage sid, ExcelPackage ird, Dictionary<string, ApexPort> channels, HashSet<string> OutPar_Dict)
        {
            SID = sid;
            IRD = ird;
            CH_Dict = channels;
            OutputParams_Dict = OutPar_Dict;
        }

        public void Process(bool directionIsIn)
        {
            var ws_in = SID.Workbook.Worksheets[directionIsIn? SID_Sheets.A429_I_PageName : SID_Sheets.A429_O_PageName];
            var ws_out = IRD.Workbook.Worksheets[directionIsIn ? IRD_Sheets.InputChannel_PageName : IRD_Sheets.OutputChannel_PageName];

            readHeaders(ws_in, ws_out);

            sortA429ByColumns(ws_in, "Parameter name", true);
            sortA429ByColumns(ws_in, "Word label", false);

            int startRow = SID_Sheets.A429_FirstDataRow;
            int outputStartRow = IRD_Worker.GetFirstNullChannelRow(ws_out, RD["Channel Name"]);                
            int countIn = 0;
            int countOut = 0;
            int countSkipped = 0;

            do
            {
                int curRowIN = startRow + countIn;
                int curRowOUT = outputStartRow + countOut;

                if (ws_in.Cells[curRowIN, SD["ICD parameter name"]].Value == null)
                    break;

                bool rowAdded = processRow(ws_out, ws_in, curRowIN, curRowOUT, directionIsIn);
                if (rowAdded)
                    countOut++;
                else
                    countSkipped++;

                countIn++;
                if (countIn % 10 == 0) Console.Write(".");
            }
            while (true);

            string outp = directionIsIn ? "Input" : "Output";
            Console.WriteLine($"\nA429 {outp} Params processed : {countIn}  Skipped: {countSkipped}");
        }

        private void sortA429ByColumns(ExcelWorksheet ws, string columnName, bool order)
        {
            int endRow = IRD_Worker.GetFirstNullRow(ws, SID_Sheets.A429_FirstDataRow, SD["Parameter name"]);
            using (ExcelRange excelRange = ws.Cells[SID_Sheets.A429_FirstDataRow, SID_Sheets.A429_HeaderColumn_First, endRow, SID_Sheets.A429_HeaderColumn_Last])
            {
                int sortColumn = SD[columnName] - SID_Sheets.A429_HeaderColumn_First;
                excelRange.Sort(sortColumn, order);
            }
        }

        Dictionary<string, int> SD = new Dictionary<string, int>();
        Dictionary<string, int> RD = new Dictionary<string, int>();        

        private void readHeaders(ExcelWorksheet ws_in, ExcelWorksheet ws_out)
        {
            SD.Clear();
            RD.Clear();
            for (int col=IRD_Sheets.Channels_HeaderColumn_First; col<= IRD_Sheets.Channels_HeaderColumn_Last;col++)
            {
                var val = ws_out.Cells[IRD_Sheets.Channels_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                RD[name] = col;
            }            
            for (int col = SID_Sheets.A429_HeaderColumn_First; col <= SID_Sheets.A429_HeaderColumn_Last; col++)
            {
                var val = ws_in.Cells[SID_Sheets.A429_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                SD[name] = col;
            }
        }

        private bool processRow(ExcelWorksheet ws_out, ExcelWorksheet sidrow, int rowin, int rowout, bool directionIsIn)
        {
            var Postfix = "C?";
            var Prefix = "S_";
            bool isQueue = false;

            var Refresh = sidrow.Cells[rowin, SD["Refresh time"]].Value.ToString();
            var ICDparName = sidrow.Cells[rowin, SD["ICD parameter name"]].Value.ToString();

            if (directionIsIn && (OutputParams_Dict.Count()>0))
            {
                var ParName = sidrow.Cells[rowin, SD["Parameter name"]].Value.ToString();

                if (!OutputParams_Dict.Contains(ParName))
                    return false;    //  Not this APP parameter 
            }

            if (Refresh == "N/A" || Refresh == "0") { Postfix = "CQ"; Prefix = "Q_"; isQueue = true; }
            else
            {
                int v50 = int.Parse(Refresh);
                double digit = (double)v50/50.0;
                Postfix = $"C{Math.Ceiling(digit)}";
            }

            var IntID = sidrow.Cells[rowin, SD["<<Interface ID>>"]].Value;
            var DirPrefix = directionIsIn ? "I" : "O";
            var ChName = $"{Prefix}{DirPrefix}_A_{IntID}_{Postfix}";

            var Word = sidrow.Cells[rowin, SD["Word label"]].Value;
            var SDI = sidrow.Cells[rowin, SD["SDI"]].Value;
            var FDS_Name = $"FDS_{Word}_{SDI}";

            var ICDparFullName = $"{ICDparName}_{SDI}";
            var par_fds_id = $"{ICDparFullName} {FDS_Name}";

            if (CH_Dict.ContainsKey(ChName))
            {
                if (ICDparFullName != null && CH_Dict[ChName].Params.Contains(par_fds_id))
                {
                    return false; //  Already have such a parameter!
                }
                //if (CH_Dict[ChName].Params.Contains(ICDparFullName))  return false;  //  Already have such a parameter!

                if (!CH_Dict[ChName].FSs.Keys.Contains(FDS_Name))
                {
                    CH_Dict[ChName].FS_Counter++;
                    CH_Dict[ChName].FSs[FDS_Name] = CH_Dict[ChName].FS_Counter;
                }

                //  new 429 label?
                int fdsCnt = (CH_Dict[ChName].FS_Counter / 4 +1 );
                CH_Dict[ChName].PortSize = 4 + fdsCnt * 20;         // 20 bytes per FDS + 4
            }
            else
            {
                CH_Dict[ChName] = new ApexPort()
                {
                    ConnectedTo = ChName,
                    FS_Counter = 0,
                    API_PortNumber = CH_Dict.Count + 1,
                    Mode = isQueue ? PortMode.Queuing : PortMode.Sampling,
                    IO = directionIsIn ? PortDirection.I : PortDirection.O,
                    PortSize = 24,                                  // 20 bytes per FDS + 4. Atleast 1 FDS is used
                    API_Freq = isQueue ? "N/A" : Refresh.ToString() // @ A.V. Krytsin: added cast to type "string",
                                                                    // changed value for queuing ports
                };
                CH_Dict[ChName].FSs[FDS_Name] = 0;
            }
            //CH_Dict[ChName].Params.Add(ICDparFullName);
            CH_Dict[ChName].Params.Add(par_fds_id);

            //  Channel Name

            ws_out.Cells[rowout, RD["Channel Name"]].Value = ChName;

            //  Message Name

            ws_out.Cells[rowout, RD["Message Name"]].Value = $"MSG_A429_{Word}_{SDI}";

            //  FDS Name

            ws_out.Cells[rowout, RD["FDS Name"]].Value = $"FDS_{Word}_{SDI}";

            //  FDS Size

            ws_out.Cells[rowout, RD["FDS Size"]].Value = 2;

            //  FS MSW

            if (isQueue)
            {
                ws_out.Cells[rowout, RD["FS MSW"]].Value = "N/A";
            }
            else
            {
                int ch_cnt = CH_Dict[ChName].FSs[FDS_Name];
                int hi = ch_cnt / 4;
                int lo = ((ch_cnt / 2) % 2);
                int fsmsw =  hi*10 + (lo == 0 ? 2:3);
                ws_out.Cells[rowout, RD["FS MSW"]].Value = fsmsw;
            }

            //  FS MSB

            if (isQueue)
            {
                ws_out.Cells[rowout, RD["FS MSB"]].Value = "N/A";
            }
            else
            {
                int ch_cnt = CH_Dict[ChName].FSs[FDS_Name];
                ws_out.Cells[rowout, RD["FS MSB"]].Value = ch_cnt % 2 == 0? 15:7;
            }

            //  Parameter Name
            
            ws_out.Cells[rowout, RD["Parameter Name"]].Value = ICDparName;

            //  MSW

            if (isQueue)
                ws_out.Cells[rowout, RD["MSW"]].Value = "N/A";
            else
            {
                int ch_cnt = CH_Dict[ChName].FSs[FDS_Name];
                int fours = ch_cnt / 4;
                int word =  ch_cnt % 4;
                var MSW = 4 + fours * 10 + 2 * word;
                ws_out.Cells[rowout, RD["MSW"]].Value = MSW;
            }

            //  MSB

            var MSB = sidrow.Cells[rowin, SD["MSB"]].Value.ToString();
            ws_out.Cells[rowout, RD["MSB"]].Value = int.Parse(MSB);

            //  Size 

            var LSB = sidrow.Cells[rowin, SD["LSB"]].Value.ToString();
            int Size = int.Parse(MSB) - int.Parse(LSB) + 1;
            ws_out.Cells[rowout, RD["Size"]].Value = Size;

            //  Format

            var Format = sidrow.Cells[rowin, SD["SSM Type"]].Value.ToString();
            ws_out.Cells[rowout, RD["Format"]].Value = Format;

            //  Value Domain

            var val_min = sidrow.Cells[rowin, SD["Physical Range MIN"]].Value;
            var val_max = sidrow.Cells[rowin, SD["Physical Range MAX"]].Value;
            if (val_min!=null && val_max != null && (val_min.ToString()!="N/A"))
            {
                ws_out.Cells[rowout, RD["Value Domain"]].Value = $"{val_min}...{val_max}";
            }
            else
            {
                ws_out.Cells[rowout, RD["Value Domain"]].Value = "N/A";
            }

            return true;
        }
    }
}
