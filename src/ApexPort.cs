﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    internal enum PortDirection
    {
        I,
        O           
    }
    internal enum PortMode
    {
        Sampling,
        Queuing
    }
    internal class ApexPort
    {
        internal string ConnectedTo;
        internal string a664vlName;
        internal string InterfaceType;
        internal PortDirection IO;
        internal PortMode Mode;
        internal int PortSize;
        internal string API_Freq; // @ A.V. Krytsin: type changed to "string" 
        internal int API_PortNumber;
        internal int FS_Counter;
        internal List<string> Params = new List<string>();
        internal Dictionary<string, int> FSs = new Dictionary<string, int>();
    }
}
