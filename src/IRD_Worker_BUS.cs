﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    class IRD_Worker_BUS
    {
        readonly ExcelPackage SID;
        readonly ExcelPackage IRD;
        readonly Dictionary<string, ApexPort> CH_Dict;
        readonly HashSet<string> OutputParams_Dict;

        readonly CultureInfo provider;

        public IRD_Worker_BUS(ExcelPackage sid, ExcelPackage ird, Dictionary<string, ApexPort> channels, HashSet<string> OutPar_Dict)
        {
            SID = sid;
            IRD = ird;
            CH_Dict = channels;
            OutputParams_Dict = OutPar_Dict;
            provider = CultureInfo.InvariantCulture;
        }

        public void Process(bool directionIsIn)
        {
            var ws_in = SID.Workbook.Worksheets[directionIsIn ? SID_Sheets.A429_I_PageName : SID_Sheets.A429_O_PageName];
            var ws_out = IRD.Workbook.Worksheets[directionIsIn ? IRD_Sheets.InputBus_PageName : IRD_Sheets.OutputBus_PageName];

            readHeaders(ws_in, ws_out);

            sortA429ByColumns(ws_in, "Parameter name", true);
            sortA429ByColumns(ws_in, "Word label", false);

            int startRow = SID_Sheets.A429_FirstDataRow;
            int outputStartRow = IRD_Worker.GetFirstNullBUSRow(ws_out, RD["Identifier"]);
            int countIn = 0;
            int countOut = 0;
            int countSkipped = 0;

            do
            {
                int curRowIN = startRow + countIn;
                int curRowOUT = outputStartRow + countOut;

                if (ws_in.Cells[curRowIN, SD["ICD parameter name"]].Value == null)
                    break;

                bool rowAdded = processRow(ws_out, ws_in, curRowIN, curRowOUT, directionIsIn);
                if (rowAdded)
                    countOut++;
                else
                    countSkipped++;

                countIn++;
                if (countIn % 10 == 0) Console.Write(".");
            }
            while (true);

            string outp = directionIsIn ? "Input" : "Output";
            Console.WriteLine($"\nA429 {outp} Params processed : {countIn}  Skipped: {countSkipped}");
        }

        private void sortA429ByColumns(ExcelWorksheet ws, string columnName, bool order)
        {
            int endRow = IRD_Worker.GetFirstNullRow(ws, SID_Sheets.A429_FirstDataRow, SD["Parameter name"]);
            using (ExcelRange excelRange = ws.Cells[SID_Sheets.A429_FirstDataRow, SID_Sheets.A429_HeaderColumn_First, endRow, SID_Sheets.A429_HeaderColumn_Last])
            {
                int sortColumn = SD[columnName] - SID_Sheets.A429_HeaderColumn_First;
                excelRange.Sort(sortColumn, order);
            }
        }

        Dictionary<string, int> SD = new Dictionary<string, int>();
        Dictionary<string, int> RD = new Dictionary<string, int>();

        private void readHeaders(ExcelWorksheet ws_in, ExcelWorksheet ws_out)
        {
            SD.Clear();
            RD.Clear();
            for (int col = IRD_Sheets.Channels_HeaderColumn_First; col <= IRD_Sheets.Channels_HeaderColumn_Last; col++)
            {
                var val = ws_out.Cells[IRD_Sheets.Channels_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                RD[name] = col;
            }
            for (int col = SID_Sheets.A429_HeaderColumn_First; col <= SID_Sheets.A429_HeaderColumn_Last; col++)
            {
                var val = ws_in.Cells[SID_Sheets.A429_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                SD[name] = col;
            }
        }

        private bool processRow(ExcelWorksheet ws_out, ExcelWorksheet sidrow, int rowin, int rowout, bool directionIsIn)
        {
            var Refresh = sidrow.Cells[rowin, SD["Refresh time"]].Value.ToString();
            var ICDparName = sidrow.Cells[rowin, SD["ICD parameter name"]].Value.ToString();

            if (directionIsIn && (OutputParams_Dict.Count() > 0))
            {
                var parName = sidrow.Cells[rowin, SD["Parameter name"]].Value.ToString();

                if (!OutputParams_Dict.Contains(parName))
                    return false;    //  Not this APP parameter 
            }

            var IntID = sidrow.Cells[rowin, SD["<<Interface ID>>"]].Value;
            var DirPrefix = directionIsIn ? "I" : "O";
            var ChName = $"{IntID}";

            var Word = sidrow.Cells[rowin, SD["Word label"]].Value;
            var SDI = sidrow.Cells[rowin, SD["SDI"]].Value;
            var FDS_Name = $"FDS_{Word}_{SDI}";

            var ICDparFullName = $"{ICDparName}_{SDI}";
            var par_fds_id = $"{ICDparFullName} {FDS_Name}";

            if (CH_Dict.ContainsKey(ChName))
            {
                if (ICDparFullName != null && CH_Dict[ChName].Params.Contains(par_fds_id))
                {
                    return false; //  Already have such a parameter!
                }

                if (!CH_Dict[ChName].FSs.Keys.Contains(FDS_Name))
                {
                    CH_Dict[ChName].FS_Counter++;
                    CH_Dict[ChName].FSs[FDS_Name] = CH_Dict[ChName].FS_Counter;
                }
            }
            else
            {
                var IntType = sidrow.Cells[rowin, SD["Interface type"]].Value;
                CH_Dict[ChName] = new ApexPort()
                {
                    ConnectedTo = ChName,
                    InterfaceType = IntType?.ToString(),
                    FS_Counter = 0,
                    API_PortNumber = CH_Dict.Count + 1,
                    IO = directionIsIn ? PortDirection.I : PortDirection.O,
                };
                CH_Dict[ChName].FSs[FDS_Name] = 0;
            }

            CH_Dict[ChName].Params.Add(par_fds_id);

            //  Identifier

            ws_out.Cells[rowout, RD["Identifier"]].Value = IntID.ToString();

            //  Message Name

            ws_out.Cells[rowout, RD["Message Name"]].Value = $"MSG_A429_{Word}_{SDI}";

            //  FDS Name

            ws_out.Cells[rowout, RD["FDS Name"]].Value = $"FDS_{Word}_{SDI}";

            //  Format

            var Format = sidrow.Cells[rowin, SD["Parameter Type"]].Value.ToString();
            ws_out.Cells[rowout, RD["Format"]].Value = Format;

            //  SSM

            var SSM = sidrow.Cells[rowin, SD["SSM Type"]].Value.ToString();
            ws_out.Cells[rowout, RD["SSM"]].Value = SSM;

            //  Parameter Name

            var ParName = sidrow.Cells[rowin, SD["Parameter name"]].Value.ToString();
            ws_out.Cells[rowout, RD["Parameter Name"]].Value = ParName;

            //  ICD Parameter Name

            ws_out.Cells[rowout, RD["ICD Parameter Name"]].Value = ICDparName;

            //  MSB

            var MSB = sidrow.Cells[rowin, SD["MSB"]].Value.ToString();
            ws_out.Cells[rowout, RD["MSB"]].Value = int.Parse(MSB);

            //  Size 

            var LSB = sidrow.Cells[rowin, SD["LSB"]].Value.ToString();
            int Size = int.Parse(MSB) - int.Parse(LSB) + 1;
            ws_out.Cells[rowout, RD["Size"]].Value = Size;

            //  Range

            var weight = sidrow.Cells[rowin, SD["MSB Weight In Units"]].Value;
            double range = 0;
            if (weight == null || weight.ToString() == "N/A")
            {
                ws_out.Cells[rowout, RD["Range"]].Value = "N/A";
            }
            else
            {
                if (double.TryParse(weight.ToString(), NumberStyles.AllowDecimalPoint, provider, out range))
                {
                    range *= 2.0;
                    ws_out.Cells[rowout, RD["Range"]].Value = range.ToString(provider);
                }
            }

            //  Type Name

            //  Значение выставляется в
            //  зависимости от типа параметра.

            //  1)Если тип параметра – «BNR», то
            //    применяется следующий алгоритм:
            //	  Если((MSB_Weight * 2) / (MSB – LSB + 1) ^ 2) % 1 = 0
            //    То поле принимает значение «Integer»
            //	  Иначе поле принимает значение «Float»
            //    
            //	2)Если тип параметра – «DD», то
            //    поле принимает значение «Boolean»
            //	3)Если тип параметра – «OPQ», то
            //    поле принимает значение «N / A»
            //	4)Если тип параметра – «BCD», то
            //    значение устанавливается согласно полю «MSB Weight In Units»
            //    на соответствующей вкладке SID «A429 Output/ Input Data». 
            //    Если значение поля «MSB Weight In Units» принимает вещественный вид типа «7999.9»,
            //	  То поле<Type Name> принимает значение «Float» Иначе поле принимает значение «Integer»

            var ptype = Format;
            string typeName = "";
            if (ptype == "BNR" && Size != 0)
            {
                if(range != 0)
                {
                    var test = (((range / Size) * (range / Size)) % 1) == 0;
                    typeName = test ? "Integer" : "Float";
                }
            }
            else
            if (ptype == "DD")
            {
                typeName = "Boolean";
            }
            else
            if (ptype == "OPQ")
            {
                typeName = "N/A";
            }
            else
            if (ptype == "BCD")
            {
                if (weight!=null && weight.ToString().Contains("."))
                    typeName = "Float";
                else
                    typeName = "Integer";
            }
            else
            if(ptype == "Alphabet")
            {
                typeName = "Opaque";
            }

            //typeName = $"{typeName} [{Size}]";

            ws_out.Cells[rowout, RD["Type Name"]].Value = typeName;

            // SIGN Bit positive direction

            var SIGN = sidrow.Cells[rowin, SD["Sign Bit Positive Direction"]].Value.ToString();
            ws_out.Cells[rowout, RD["SIGN Bit positive direction"]].Value = SIGN;

            return true;
        }
    }
}
