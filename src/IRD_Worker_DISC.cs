﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    class IRD_Worker_DISC
    {
        readonly ExcelPackage SID;
        readonly ExcelPackage IRD;
        readonly Dictionary<string, ApexPort> CH_Dict;
        readonly HashSet<string> OutputParams_Dict;

        private readonly string PinProgPortName = "S_I_DISC_PP";
        private readonly string PinProgChannelName = "S_I_DISC_PINPROG";
        private readonly bool SpecialPPPort = false;


        public IRD_Worker_DISC(ExcelPackage sid, ExcelPackage ird, Dictionary<string, ApexPort> channels, HashSet<string> OutPar_Dict, bool ppport)
        {
            SID = sid;
            IRD = ird;
            CH_Dict = channels;
            OutputParams_Dict = OutPar_Dict;
            SpecialPPPort = ppport;
        }

        public void Process(bool directionIsIn)
        {
            var ws_in = SID.Workbook.Worksheets[SID_Sheets.Discrete_PageName];
            var ws_out = IRD.Workbook.Worksheets[directionIsIn ? IRD_Sheets.InputChannel_PageName : IRD_Sheets.OutputChannel_PageName];

            readHeaders(ws_in, ws_out);

            int startRow = SID_Sheets.DISC_FirstDataRow;
            int outputStartRow = IRD_Worker.GetFirstNullChannelRow(ws_out, RD["Channel Name"]);
            int countIn = 0;
            int countOut = 0;
            int countSkipped = 0;

            do
            {
                int curRowIN = startRow + countIn;
                int curRowOUT = outputStartRow + countOut;

                if (ws_in.Cells[curRowIN, SD["Parameter Name"]].Value == null)
                    break;

                bool rowAdded = processRow(ws_out, ws_in, curRowIN, curRowOUT, directionIsIn);
                if (rowAdded)
                    countOut++;
                else
                    countSkipped++;

                countIn++;
                if (countIn % 10 == 0) Console.Write(".");
            }
            while (true);

            string outp = directionIsIn ? "Input" : "Output";
            Console.WriteLine($"\nDISC {outp} Params processed : {countIn}  Skipped: {countSkipped}");
        }

        Dictionary<string, int> SD = new Dictionary<string, int>();
        Dictionary<string, int> RD = new Dictionary<string, int>();

        private void readHeaders(ExcelWorksheet ws_in, ExcelWorksheet ws_out)
        {
            SD.Clear();
            RD.Clear();
            for (int col=IRD_Sheets.Channels_HeaderColumn_First; col<= IRD_Sheets.Channels_HeaderColumn_Last; col++)
            {
                var val = ws_out.Cells[IRD_Sheets.Channels_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                RD[name] = col;
            }            
            for (int col = SID_Sheets.DISC_HeaderColumn_First; col <= SID_Sheets.DISC_HeaderColumn_Last; col++)
            {
                var val = ws_in.Cells[SID_Sheets.DISC_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                SD[name] = col;
            }
        }

        private bool processRow(ExcelWorksheet ws_out, ExcelWorksheet sidrow, int rowin, int rowout, bool directionIsIn)
        {
            var Postfix = "C1";
            var Prefix = "S_";

            bool parIOIsIn = sidrow.Cells[rowin, SD["I/O"]].Value.ToString() == "I";
            var ICDparName = sidrow.Cells[rowin, SD["Bus Name"]].Value.ToString();
            var AppID = sidrow.Cells[rowin, SD["Application"]].Value.ToString().Substring(2, 2);

            if (ICDparName.StartsWith("DSI_GNDO"))
            {
                return false;       //  Skipping DSI_GND
            }

            if (SpecialPPPort && ICDparName.StartsWith("PIN_PROG"))
            {
                return processPinProgRow(ws_out, sidrow, rowin, rowout, directionIsIn);
            }

            var Identity = "DISCRETE";
            var DirPrefix = directionIsIn ? "I" : "O";
            var ChName = $"{Prefix}{DirPrefix}_D_{AppID}_{Identity}_{Postfix}";

            if (directionIsIn != parIOIsIn)
                return false;       //  Skipping direction

            if (CH_Dict.ContainsKey(ChName))
            {
                if (CH_Dict[ChName].Params.Contains(ICDparName))
                    return false;   //  Already have such a parameter!
                CH_Dict[ChName].FS_Counter++;
                int fdsCnt = (CH_Dict[ChName].FS_Counter / 4 + 1);
                //CH_Dict[ChName].PortSize = 4 + fdsCnt * 20;         // 20 bytes per FDS + 4
                CH_Dict[ChName].PortSize = 128;
            }
            else
            {
                CH_Dict[ChName] = new ApexPort()
                {
                    ConnectedTo = ChName,
                    FS_Counter = 0,
                    API_PortNumber = CH_Dict.Count + 1,
                    Mode = PortMode.Sampling,
                    IO = directionIsIn ? PortDirection.I : PortDirection.O,
                    PortSize = 128,                                  
                    API_Freq = "50" // @ A.V. Krytsin: rvalue type changed to "string" 
                };
            }
            CH_Dict[ChName].Params.Add(ICDparName);

            //  Channel Name

            ws_out.Cells[rowout, RD["Channel Name"]].Value = ChName;

            //  Message Name

            var MsgName = $"MSG_{AppID}_{Identity}_{DirPrefix}";
            ws_out.Cells[rowout, RD["Message Name"]].Value = MsgName;

            //  FDS Name

            var FDS_Name = $"FDS_{Identity}_{CH_Dict[ChName].FS_Counter.ToString("00")}";
            ws_out.Cells[rowout, RD["FDS Name"]].Value = FDS_Name;

            //  FDS Size

            ws_out.Cells[rowout, RD["FDS Size"]].Value = 2;


            //  FS MSW

            {
                int ch_cnt = CH_Dict[ChName].FS_Counter;
                int hi = ch_cnt / 4;
                int lo = ((ch_cnt / 2) % 2);
                int fsmsw = hi * 10 + (lo == 0 ? 2 : 3);
                ws_out.Cells[rowout, RD["FS MSW"]].Value = fsmsw;
            }

            //  FS MSB

            {
                int ch_cnt = CH_Dict[ChName].FS_Counter;
                ws_out.Cells[rowout, RD["FS MSB"]].Value = ch_cnt % 2 == 0 ? 15 : 7;
            }

            //  Parameter Name

            ws_out.Cells[rowout, RD["Parameter Name"]].Value = ICDparName;

            //  Format

            ws_out.Cells[rowout, RD["Format"]].Value = "bool";

            //  MSW

            {
                int ch_cnt = CH_Dict[ChName].FS_Counter;
                int fours = ch_cnt / 4;
                int word = ch_cnt % 4;
                var MSW = 4 + fours * 10 + 2 * word + 1;
                ws_out.Cells[rowout, RD["MSW"]].Value = MSW;
            }

            //  MSB

            ws_out.Cells[rowout, RD["MSB"]].Value = 0;

            //  Size 

            //ws_out.Cells[rowout, RD["Size"]].Value = null;
            ws_out.Cells[rowout, RD["Size"]].Value = "N/A";

            //  Value Domain

            //ws_out.Cells[rowout, RD["Value Domain"]].Value = null;
            ws_out.Cells[rowout, RD["Value Domain"]].Value = "N/A";

            return true;
        }

        private void CreatePinProgPort()
        {
            string PP_Port = PinProgPortName;

            if (!CH_Dict.ContainsKey(PP_Port))
            {
                CH_Dict[PP_Port] = new ApexPort
                {
                    API_Freq = "50", // @ A.V. Krytsin: rvalue type changed to "string" 
                    PortSize = 128,
                    IO = PortDirection.I,
                    API_PortNumber = CH_Dict.Count + 1,
                    Mode = PortMode.Sampling,
                    ConnectedTo = PinProgChannelName
                };

                Console.WriteLine($"\nSpecial port created: {PP_Port}");
            }
        }

        private bool processPinProgRow(ExcelWorksheet ws_out, ExcelWorksheet sidrow, int rowin, int rowout, bool directionIsIn)
        {
            CreatePinProgPort();
            var ChName = PinProgChannelName;
            var ICDparName = sidrow.Cells[rowin, SD["Bus Name"]].Value.ToString();

            if (CH_Dict.ContainsKey(PinProgPortName))
            {
                if (CH_Dict[PinProgPortName].Params.Contains(ICDparName))
                    return false; //  Already have such a parameter!
            }

            CH_Dict[PinProgPortName].Params.Add(ICDparName);

            //  Channel Name

            ws_out.Cells[rowout, RD["Channel Name"]].Value = ChName;

            //  Message Name

            var MsgName = $"MSG_DISC_PP";
            ws_out.Cells[rowout, RD["Message Name"]].Value = MsgName;

            //  FDS Name

            var FDS_Name = $"FDS_PIN_PROG";
            ws_out.Cells[rowout, RD["FDS Name"]].Value = FDS_Name;

            //  FDS Size

            ws_out.Cells[rowout, RD["FDS Size"]].Value = 8;

            //  FS MSW

            ws_out.Cells[rowout, RD["FS MSW"]].Value = 2;

            //  FS MSB

            ws_out.Cells[rowout, RD["FS MSB"]].Value = 15;
 
            //  Parameter Name

            ws_out.Cells[rowout, RD["Parameter Name"]].Value = ICDparName;

            //  Format

            ws_out.Cells[rowout, RD["Format"]].Value = "bool";

            //  MSW

            {
                int ch_cnt = CH_Dict[PinProgPortName].Params.Count;
                int fours = ch_cnt / 4;
                int word = ch_cnt % 4;
                var MSW = 4 + fours * 10 + 2 * word + 1;
                if (ICDparName.Contains("_PARITY"))
                    MSW = 5;
                ws_out.Cells[rowout, RD["MSW"]].Value = MSW;
            }

            //  MSB

            ws_out.Cells[rowout, RD["MSB"]].Value = 0;

            //  Size 

            //ws_out.Cells[rowout, RD["Size"]].Value = null;
            ws_out.Cells[rowout, RD["Size"]].Value = "N/A";

            //  Value Domain

            //ws_out.Cells[rowout, RD["Value Domain"]].Value = null;
            ws_out.Cells[rowout, RD["Value Domain"]].Value = "N/A";

            return true;
        }
    }
}
