﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    public class IRD_Sheets
    {
        public const string OutputChannel_PageName = "Output Channel Definition";
        public const string InputChannel_PageName = "Input Channel Definition";
        public const int Channels_HeaderColumn_First = 1;
        public const int Channels_HeaderColumn_Last = 12;
        public const int Channels_HeaderRow = 11;
        public const int OutputChannel_FirstRow = 12;

        public const string APIDef_PageName = "API Definition";
        public const int APIDef_HeaderColumn_First = 1;
        public const int APIDef_HeaderColumn_Last = 12;
        public const int APIDef_HeaderRow = 10;
        public const int APIDef_FirstRow = 11;

        public const string OutputBus_PageName = "Output Bus Definition";
        public const string InputBus_PageName = "Input Bus Definition";
        public const int BusIO_HeaderColumn_First = 1;
        public const int BusOP_HeaderColumn_Last = 12;
        public const int BusIO_HeaderRow = 11;
        public const int BusIO_FirstRow = 12;

        public const string BusDef_PageName = "Bus Definition";
        public const int BusDef_HeaderColumn_First = 1;
        public const int BusDef_HeaderColumn_Last = 12;
        public const int BusDef_HeaderRow = 10;
        public const int BusDef_FirstRow = 11;

    }
}
