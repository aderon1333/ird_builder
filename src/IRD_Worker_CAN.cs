﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    class IRD_Worker_CAN
    {
        readonly ExcelPackage SID;
        readonly ExcelPackage IRD;
        readonly Dictionary<string, ApexPort> CH_Dict;
        readonly HashSet<string> OutputParams_Dict;

        Dictionary<string, int> BusInfo = new Dictionary<string, int>();

        public IRD_Worker_CAN(ExcelPackage sid, ExcelPackage ird, Dictionary<string, ApexPort> channels, HashSet<string> OutPar_Dict)
        {
            SID = sid;
            IRD = ird;
            CH_Dict = channels;
            OutputParams_Dict = OutPar_Dict;
        }

        public void Process()
        {
            var ws_in = SID.Workbook.Worksheets[SID_Sheets.CAN_PageName];
            var ws_bus = SID.Workbook.Worksheets[SID_Sheets.CAN_Bus_PageName];
            var ws_out = IRD.Workbook.Worksheets[IRD_Sheets.InputChannel_PageName];
            
            readHeaders(ws_in, ws_out, ws_bus);

            if (BusD.Count == 0)
            {
                Console.WriteLine($"\nCAN couldn't read header");
                return;
            }

            readBusInfo(ws_bus, BusInfo);

            int startRow = SID_Sheets.CAN_FirstDataRow;
            int outputStartRow = IRD_Worker.GetFirstNullChannelRow(ws_out, RD["Channel Name"]);
            int countIn = 0;
            int countOut = 0;
            int countSkipped = 0;

            do
            {
                int curRowIN = startRow + countIn;
                int curRowOUT = outputStartRow + countOut;

                if (ws_in.Cells[curRowIN, SD["ICD Parameter name"]].Value == null)
                    break;

                bool rowAdded = processRow(ws_out, ws_in, curRowIN, curRowOUT);
                if (rowAdded)
                    countOut++;
                else
                    countSkipped++;

                countIn++;
                if (countIn % 10 == 0) Console.Write(".");
            }
            while (true);

            string outp = "Input";
            Console.WriteLine($"\nCAN {outp} Params processed : {countIn}  Skipped: {countSkipped}");
        }

        private void readBusInfo(ExcelWorksheet ws_bus, Dictionary<string, int> busInfo)
        {
            int startRow = SID_Sheets.CAN_Bus_FirstDataRow;
            busInfo.Clear();
            int countIn = 0;
            do
            {
                int curRowIN = startRow + countIn;

                if (ws_bus.Cells[curRowIN, BusD["Name"]].Value == null)
                    break;

                var SFID = ws_bus.Cells[curRowIN, BusD["S. FID (HEX)"]].Value.ToString();
                var Refresh = ws_bus.Cells[curRowIN, BusD["Refresh rate, ms"]].Value;

                int Refresh_int = int.Parse(Refresh.ToString());

                busInfo[SFID] = Refresh_int;

                countIn++;
            }
            while (true);
        }

        Dictionary<string, int> SD = new Dictionary<string, int>();
        Dictionary<string, int> RD = new Dictionary<string, int>();
        Dictionary<string, int> BusD = new Dictionary<string, int>();

        private void readHeaders(ExcelWorksheet ws_in, ExcelWorksheet ws_out, ExcelWorksheet ws_bus)
        {
            SD.Clear();
            RD.Clear();
            BusD.Clear();
            for (int col=IRD_Sheets.Channels_HeaderColumn_First; col<= IRD_Sheets.Channels_HeaderColumn_Last; col++)
            {
                var val = ws_out.Cells[IRD_Sheets.Channels_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                RD[name] = col;
            }            
            for (int col = SID_Sheets.CAN_HeaderColumn_First; col <= SID_Sheets.CAN_HeaderColumn_Last; col++)
            {
                var val = ws_in.Cells[SID_Sheets.CAN_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                name += SD.ContainsKey(name) ? "_ADD" : "";
                SD[name] = col;
            }
            for (int col = SID_Sheets.CAN_Bus_HeaderColumn_First; col <= SID_Sheets.CAN_Bus_HeaderColumn_Last; col++)
            {
                var val = ws_bus.Cells[SID_Sheets.CAN_Bus_HeaderRow, col].Value;
                if (val == null) continue;
                string name = val.ToString().Trim();
                BusD[name] = col;
            }
        }

        private bool processRow(ExcelWorksheet ws_out, ExcelWorksheet sidrow, int rowin, int rowout)
        {
            var Postfix = "C?";
            var Prefix = "S_";
            var Mid = "MAIN";

            if (OutputParams_Dict.Count() > 0)
            {
                var ParName = sidrow.Cells[rowin, SD["Parameter name"]].Value.ToString();
                if (!OutputParams_Dict.Contains(ParName))
                    return false;    //  Not this APP parameter 
            }

            var ICDparName = sidrow.Cells[rowin, SD["ICD Parameter name"]].Value.ToString();
            var SF1 = sidrow.Cells[rowin, SD["S.FID"]].Value;
            var SF2 = sidrow.Cells[rowin, SD["S.FID_ADD"]].Value;

            if (SF1 != null && SF1.ToString() == "") SF1 = null;

            var sfid = SF1 != null ? SF1.ToString() : SF2.ToString();
            var dfid = sidrow.Cells[rowin, SD["D.FID"]].Value.ToString();

            if (SF1 == null) Mid = "ADD";

            int v50 = BusInfo[sfid];
            double digit = (double)v50 / 50.0;
            Postfix = $"C{Math.Ceiling(digit)}";

            var DirPrefix = "I";
            var ChName = $"{Prefix}{DirPrefix}_C_{dfid}_{sfid}_{Mid}_{Postfix}";

            if (CH_Dict.ContainsKey(ChName))
            {
                if (CH_Dict[ChName].Params.Contains(ICDparName))
                    return false;   //  Already have such a parameter!
                CH_Dict[ChName].FS_Counter++;
            }
            else
            {
                CH_Dict[ChName] = new ApexPort()
                {
                    ConnectedTo = ChName,
                    FS_Counter = 0,
                    API_PortNumber = CH_Dict.Count + 1,
                    Mode = PortMode.Sampling,
                    IO = PortDirection.I,
                    PortSize = 16,                                  
                    API_Freq = BusInfo[sfid].ToString() // @ A.V. Krytsin: added cast to type "string" 
                };
            }
            CH_Dict[ChName].Params.Add(ICDparName);

            //  Channel Name

            ws_out.Cells[rowout, RD["Channel Name"]].Value = ChName;

            //  Message Name

            var MsgName = $"MSG_CAN_{dfid}_{sfid}_{Mid}";
            ws_out.Cells[rowout, RD["Message Name"]].Value = MsgName;

            //  FDS Name

            var FDS_Name = $"FDS_CAN_{dfid}_{sfid}_{Mid}";
            ws_out.Cells[rowout, RD["FDS Name"]].Value = FDS_Name;

            //  FDS Size

            ws_out.Cells[rowout, RD["FDS Size"]].Value = 4;

            //  FS MSW

            ws_out.Cells[rowout, RD["FS MSW"]].Value = 2;

            //  FS MSB

            ws_out.Cells[rowout, RD["FS MSB"]].Value = 15;

            //  Parameter Name

            ws_out.Cells[rowout, RD["Parameter Name"]].Value = ICDparName;

            //  Format

            var dataType = sidrow.Cells[rowin, SD["Data type"]].Value.ToString();  
            bool isBool = dataType == "ENUM";
            ws_out.Cells[rowout, RD["Format"]].Value = isBool ? "bool" : "opaque";

            //  MSW

            {
                int ch_cnt = CH_Dict[ChName].FS_Counter;
                int fours = ch_cnt / 4;
                int word = ch_cnt % 4;
                var MSW = 4 + fours * 10 + 2 * word;
                ws_out.Cells[rowout, RD["MSW"]].Value = MSW;
            }

            //  MSB

            if (!isBool)
                ws_out.Cells[rowout, RD["MSB"]].Value = 15;

            //  Size 

            if (!isBool)
                ws_out.Cells[rowout, RD["Size"]].Value = 32; 

            //  Value Domain

            //ws_out.Cells[rowout, RD["Value Domain"]].Value = null;
            ws_out.Cells[rowout, RD["Value Domain"]].Value = "N/A";

            return true;
        }
    }
}
