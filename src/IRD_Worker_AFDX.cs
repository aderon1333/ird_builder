﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinParseXLS
{
    class MessageInfo
    {
        internal int Freq { get; set; }
        internal int Size { get; set; }
        internal string AFDXPort { get; set; }
        internal string Name { get; set; }
    }

    class IRD_Worker_AFDX
    {
        readonly ExcelPackage SID;
        readonly ExcelPackage IRD;
        readonly Dictionary<string, ApexPort> CH_Dict;
        readonly HashSet<string> OutputParams_Dict;

        public IRD_Worker_AFDX(ExcelPackage sid, ExcelPackage ird, Dictionary<string, ApexPort> channels, HashSet<string> OutPar_Dict)
        {
            SID = sid;
            IRD = ird;
            CH_Dict = channels;
            OutputParams_Dict = OutPar_Dict;
        }

        public void Process(bool directionIsIn)
        {
            var ws_in = SID.Workbook.Worksheets[directionIsIn? SID_Sheets.AFDX_I_PageName : SID_Sheets.AFDX_O_PageName];
            var ws_m = SID.Workbook.Worksheets[SID_Sheets.AFDX_Message_PageName];
            var ws_out = IRD.Workbook.Worksheets[directionIsIn ? IRD_Sheets.InputChannel_PageName : IRD_Sheets.OutputChannel_PageName];

            readHeaders(ws_in, ws_out, ws_m);
            readMessageDefinitions(ws_m);

            int startRow = SID_Sheets.AFDX_FirstDataRow;
            int outputStartRow = IRD_Worker.GetFirstNullChannelRow(ws_out, RD["Channel Name"]);
            int countIn = 0;
            int countOut = 0;
            int countSkipped = 0;

            do
            {
                int curRowIN = startRow + countIn;
                int curRowOUT = outputStartRow + countOut;

                if (ws_in.Cells[curRowIN, SD["Message Name"]].Value == null)
                    break;

                bool rowAdded = processRow(ws_out, ws_in, curRowIN, curRowOUT, directionIsIn);
                if (rowAdded)
                    countOut++;
                else
                    countSkipped++;

                countIn++;
                if (countIn % 10 == 0) Console.Write(".");
            }
            while (true);

            string outp = directionIsIn ? "Input" : "Output";
            Console.WriteLine($"\nAFDX {outp} Params processed : {countIn}  Skipped: {countSkipped}");
        }

        private void readMessageDefinitions(ExcelWorksheet ws_m)
        {
            int startRow = SID_Sheets.AFDX_M_FirstDataRow;
            int countIn = 0;

            do
            {
                int curRowIN = startRow + countIn;

                var vlName = ws_m.Cells[curRowIN, SDM["VL Name"]].Value;
                var eqpName = ws_m.Cells[curRowIN, SDM["Equipment"]].Value;
                var msqName = ws_m.Cells[curRowIN, SDM["Message Name"]].Value;
                var msgIO = ws_m.Cells[curRowIN, SDM["I/0"]].Value;

                string fullName = $"{eqpName}_{vlName}_{msqName}_{msgIO}";
                var freqVal = ws_m.Cells[curRowIN, SDM["Freq"]].Value;
                var msgSize = ws_m.Cells[curRowIN, SDM["Size"]].Value;
                var msgPort = ws_m.Cells[curRowIN, SDM["AFDX Port"]].Value;

                if (msgPort == null) msgPort = "";
                if (msqName == null)
                    break;

                countIn++;

                MessageInfo mi = new MessageInfo();
                if (freqVal != null && freqVal.ToString() != "")
                    mi.Freq = int.Parse(freqVal.ToString());
                else
                    mi.Freq = 0;
                mi.Size = int.Parse(msgSize.ToString());
                mi.AFDXPort = msgPort.ToString();
                mi.Name = msqName.ToString();

                MessageInfos[fullName.ToString()] = mi;
            }
            while (true);
        }

        Dictionary<string, int> SD = new Dictionary<string, int>();
        Dictionary<string, int> SDM = new Dictionary<string, int>();
        Dictionary<string, int> RD = new Dictionary<string, int>();
        Dictionary<string, MessageInfo> MessageInfos = new Dictionary<string, MessageInfo>();

        private void readHeaders(ExcelWorksheet ws_in, ExcelWorksheet ws_out, ExcelWorksheet ws_m_in)
        {
            SD.Clear();
            RD.Clear();
            SDM.Clear();
            for (int col=IRD_Sheets.Channels_HeaderColumn_First; col<= IRD_Sheets.Channels_HeaderColumn_Last; col++)
            {
                var val = ws_out.Cells[IRD_Sheets.Channels_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                RD[name] = col;
            }            
            for (int col = SID_Sheets.AFDX_HeaderColumn_First; col <= SID_Sheets.AFDX_HeaderColumn_Last; col++)
            {
                var val = ws_in.Cells[SID_Sheets.AFDX_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                name += SD.ContainsKey(name) ? "_ICD" : "";
                SD[name] = col;
            }
            for (int col = SID_Sheets.AFDX_M_HeaderColumn_First; col <= SID_Sheets.AFDX_M_HeaderColumn_Last; col++)
            {
                var val = ws_m_in.Cells[SID_Sheets.AFDX_M_HeaderRow, col].Value;
                if (val == null) break;
                string name = val.ToString().Trim();
                SDM[name] = col;
            }
        }

        private bool processRow(ExcelWorksheet ws_out, ExcelWorksheet sidrow, int rowin, int rowout, bool directionIsIn)
        {
            var Postfix = "C?";
            var Prefix = "S_";
            bool isQueue = false;

            var MsgName = sidrow.Cells[rowin, SD["Message Name"]].Value.ToString();
            var vlName = sidrow.Cells[rowin, SD["VL Name"]].Value;
            string srcEqColumn = "Equipment";
            var eqpName = sidrow.Cells[rowin, SD[srcEqColumn]].Value;
            var msgIO = directionIsIn? "I":"O";

            string fullMsgName = $"{eqpName}_{vlName}_{MsgName}_{msgIO}";

            if (!MessageInfos.Keys.Contains(fullMsgName))
            {
                Console.Write('X');
                return false;
            }

            var MsgVals = MessageInfos[fullMsgName];
            var Refresh = MsgVals.Freq;
            
            if (Refresh == 0)
            {
                Postfix = "CQ";
                Prefix = "Q_";
                isQueue = true;
            }
            else
            {
                double digit = (double)Refresh / 50.0;
                Postfix = $"C{Math.Ceiling(digit)}";
            }

            if (directionIsIn && (OutputParams_Dict.Count() > 0))
            {
                var ParName = sidrow.Cells[rowin, SD["Parameter name"]].Value;
                if (ParName!=null && !OutputParams_Dict.Contains(ParName.ToString()))
                    return false;    //  Not this APP parameter 
            }

            var Identity = sidrow.Cells[rowin, SD["Identity"]].Value;
            var DirPrefix = directionIsIn ? "I" : "O";
            var ChName = $"{Prefix}{DirPrefix}_X_{Identity}_{MsgVals.AFDXPort}_{Postfix}";

            var FDS_Name = sidrow.Cells[rowin, SD["Name"]].Value;
            if (FDS_Name == null) return false;

            var ICDparVal = sidrow.Cells[rowin, SD["Name_ICD"]].Value;

            var par_fds_id = $"{ICDparVal.ToString()} {FDS_Name.ToString()}";

            if (CH_Dict.ContainsKey(ChName))
            {
                if (ICDparVal != null && CH_Dict[ChName].Params.Contains(par_fds_id))
                {
                    return false; //  Already have such a parameter!
                }
                //if (ICDparVal!=null && CH_Dict[ChName].Params.Contains(ICDparVal.ToString()))
                //{
                //    if (CH_Dict[ChName].FSs.ContainsKey(FDS_Name.ToString()))
                //    return false; //  Already have such a parameter!
                //}
                //CH_Dict[ChName].FSs[FDS_Name.ToString()] = 0;
            }
            else
            {
                var mi = MsgVals;
                var vlname = sidrow.Cells[rowin, SD["VL Name"]].Value?.ToString();
                CH_Dict[ChName] = new ApexPort()
                {
                    a664vlName = vlname,
                    ConnectedTo = ChName,
                    FS_Counter = 0,
                    API_PortNumber = CH_Dict.Count + 1,
                    Mode = isQueue ? PortMode.Queuing : PortMode.Sampling,
                    IO = directionIsIn ? PortDirection.I : PortDirection.O,
                    PortSize = mi.Size,
                    API_Freq = isQueue ? "N/A" : mi.Freq.ToString() // @ A.V. Krytsin: added cast to type "string",
                                                                    // changed value for queuing ports
                };
            }

            //if (ICDparVal!=null)
            //    CH_Dict[ChName].Params.Add(ICDparVal.ToString());
            CH_Dict[ChName].Params.Add(par_fds_id);

            //  Channel Name

            ws_out.Cells[rowout, RD["Channel Name"]].Value = ChName;

            //  Message Name

            ws_out.Cells[rowout, RD["Message Name"]].Value = MsgName;

            //  FDS Name

            ws_out.Cells[rowout, RD["FDS Name"]].Value = FDS_Name;

            //  FS MSW

            var FS_MSW = sidrow.Cells[rowin, SD["FS MSW"]].Value;
            if (FS_MSW!=null) ws_out.Cells[rowout, RD["FS MSW"]].Value = int.Parse(FS_MSW.ToString());

            //  FS MSB

            var FS_MSB = sidrow.Cells[rowin, SD["FS MSB"]].Value;
            if (FS_MSB != null) ws_out.Cells[rowout, RD["FS MSB"]].Value = int.Parse(FS_MSB.ToString());

            //  FDS Size

            var FS_Size = sidrow.Cells[rowin, SD["Size"]].Value;
            if (FS_Size != null) ws_out.Cells[rowout, RD["FDS Size"]].Value = int.Parse(FS_Size.ToString());

            //  Parameter Name

            ws_out.Cells[rowout, RD["Parameter Name"]].Value = ICDparVal;

            //  MSW

            var MSW = sidrow.Cells[rowin, SD["MSW"]].Value;
            if (MSW != null) ws_out.Cells[rowout, RD["MSW"]].Value = int.Parse(MSW.ToString());

            //  MSB

            var MSB = sidrow.Cells[rowin, SD["MSB"]].Value;
            if (MSB != null) ws_out.Cells[rowout, RD["MSB"]].Value = int.Parse(MSB.ToString());

            //  Size 

            var Size_ICD = sidrow.Cells[rowin, SD["Size_ICD"]].Value;
            if (Size_ICD != null && Size_ICD.ToString() != "")
            {
                ws_out.Cells[rowout, RD["Size"]].Value = int.Parse(Size_ICD.ToString());
            }
            else
            {
                ws_out.Cells[rowout, RD["Size"]].Value = "N/A";
            }


            //  Format

            var Format = sidrow.Cells[rowin, SD["type"]].Value;
            ws_out.Cells[rowout, RD["Format"]].Value = Format.ToString();

            //  Value Domain

            var Value_Domain = sidrow.Cells[rowin, SD["Value Domain"]].Value;
            if (Value_Domain!=null && Value_Domain.ToString()!="N/A")
            {
                ws_out.Cells[rowout, RD["Value Domain"]].Value = Value_Domain;
            }
            else
            {
                ws_out.Cells[rowout, RD["Value Domain"]].Value = "N/A";
            }
                
            return true;
        }
    }
}
