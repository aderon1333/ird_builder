# IRD_BUILDER

Tool for IRD (interface requirements) generation 

# Inputs

- SID.xlsx - system interfaces document
- IRD_Blank.xlsx - template 

# Output

- IRD.xlsx - interface requirements document
